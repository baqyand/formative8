package formative2;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String[] regex = {"L","i","p","o","s"};
        String[] temp = new String[regex.length+1];
        int[] replace = {7,1,8,9,5};
        String str = LoremIpsum();
        temp[0]=str;
        for (int i = 0; i < regex.length; i++){
            temp[i + 1] = temp[i].replaceAll(regex[i],String.valueOf(replace[i]));
        }
        System.out.println("OTW membuat file.. hihihi");
        try {
            Thread.sleep(3_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try{
            FileWriter day821 = new FileWriter("day821.txt");
            FileWriter day822 = new FileWriter("day822.txt");

            String withAngka = temp[temp.length-1].replaceAll("\\D+"," ");
            String withHuruf = temp[temp.length-1].replaceAll("\\d+","");

            day821.write(withAngka);
            day822.write(withHuruf);

            System.out.println("File Berhasil di buat");

            day821.close();
            day822.close();

        }catch (IOException e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

    }

    public static String LoremIpsum(){
        return " Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
                " Maecenas dictum nisl sed mi tempus tristique. Suspendisse eu convallis nunc. " +
                "In hac habitasse platea dictumst. Mauris placerat quis nulla ac rhoncus. Mauris blandit nisl" +
                " sit amet facilisis rhoncus. Suspendisse et posuere neque. Quisque sapien lectus," +
                " posuere id nulla eget, convallis mollis tellus. Duis convallis sapien" +
                " ut nunc facilisis fermentum. Nulla placerat massa quis mattis rutrum." +
                " Interdum et malesuada fames ac ante ipsum primis in faucibus." +
                " Aenean scelerisque luctus eros ac volutpat..";
    }
}
