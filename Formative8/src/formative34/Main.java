package formative34;

import java.time.LocalDate;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ReminderCust remind = new ReminderCust();
        NewCust newCust = new NewCust();
        Scanner input = new Scanner(System.in);
        String nama;
        LocalDate localDate;

        System.out.print("pelanggan :  lama / baru : ");
        String pelanggan = input.nextLine();

        if (pelanggan.equalsIgnoreCase("lama")) {
            System.out.println("Silakan Isi nama anda");
            nama = input.nextLine();
            remind.setName(nama);
            System.out.println("silakan isi registerdatenya YYYY-MM-DD ");
            localDate = LocalDate.parse(input.nextLine());
            remind.setRemindDate(localDate);
            System.out.println("isi reminder datenya : ");
            int reminder = input.nextInt();
            remind.setReminder(reminder);
            remind.hitungPembayaran();
        } else if (pelanggan.equalsIgnoreCase("baru")) {
            System.out.println("silakan isi nama anda : ");
            nama = input.nextLine();
            newCust.setNama(nama);
            System.out.println("silakan isi tanggal YYYY-MM-DD ");
            localDate = LocalDate.parse(input.nextLine());
            newCust.setRegisterDate(String.valueOf(localDate));
            newCust.regis();
        } else {
            System.out.println("silakan isi yang benar");
        }
    }
}
