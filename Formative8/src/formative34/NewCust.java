package formative34;

import java.time.LocalDate;

public class NewCust {

    String nama;
    String registerDate;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    void regis(){
        LocalDate date = LocalDate.parse(getRegisterDate());

        System.out.println(getNama() + " anda berlangganan pada tangga : " + date);
        for (int i = 1; i <= 12; i++){
            if (i < 2){
                System.out.println("anda gratis 3 bulan ");
            }
            if (i > 4){
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(date.plusMonths(i).minusDays(7) + " -> reminder untuk membayar mas " + getNama());
                continue;
            }
        }
    }
}
