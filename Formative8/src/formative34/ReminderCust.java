package formative34;

import java.time.LocalDate;

public class ReminderCust {
    LocalDate remindDate;
    int reminder;
    int price = 153000;
    String name;


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRemindDate(LocalDate remindDate) {
        this.remindDate = remindDate;
    }

    public LocalDate getRemindDate() {
        return remindDate;
    }

    public void setReminder(int reminder) {
        this.reminder = reminder;
    }

    public int getReminder() {
        return reminder;
    }

    void hitungPembayaran() {
        System.out.println("Tanggal regis : " + getRemindDate());
        for (int i = 1; i <= 12; i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(getRemindDate().plusMonths(i).minusDays(getReminder()) + " -> email reminder dikirim ke pelanggan " + getName());
            System.out.println(getRemindDate().plusMonths(i) + "\tINVOICE\t\t" + price);
        }
    }
}
